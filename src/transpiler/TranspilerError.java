package transpiler;

public class TranspilerError extends Exception {

  public TranspilerError(String msg) {
    super("CxxTranspiler Error: " + msg);
  }
}
