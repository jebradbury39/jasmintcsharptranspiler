package transpiler;

import ast.AbstractExpression;
import ast.AbstractLvalue;
import ast.AbstractStatement;
import ast.ArrayInitExpression;
import ast.AssignmentStatement;
import ast.Ast;
import ast.BinaryExpression;
import ast.BlockStatement;
import ast.BoolExpression;
import ast.BracketExpression;
import ast.CastExpression;
import ast.CharExpression;
import ast.ClassDeclaration;
import ast.ClassDeclaration.ClassLine;
import ast.ConditionalStatement;
import ast.DeclarationStatement;
import ast.DeleteStatement;
import ast.DereferenceExpression;
import ast.DotExpression;
import ast.EnumDeclaration;
import ast.Expression;
import ast.ExpressionStatement;
import ast.FfiExpression;
import ast.FfiStatement;
import ast.FloatExpression;
import ast.ForStatement;
import ast.Function;
import ast.Function.MetaType;
import ast.FunctionCallExpression;
import ast.FunctionCallStatement;
import ast.IdentifierExpression;
import ast.InstanceofExpression;
import ast.IntegerExpression;
import ast.LambdaExpression;
import ast.LeftUnaryOpExpression;
import ast.Lvalue;
import ast.LvalueBracket;
import ast.LvalueDereference;
import ast.LvalueDot;
import ast.LvalueFfi;
import ast.LvalueId;
import ast.MapInitExpression;
import ast.MultiExpression;
import ast.MultiLvalue;
import ast.NewClassInstanceExpression;
import ast.NewExpression;
import ast.Program;
import ast.ReferenceExpression;
import ast.ReturnStatement;
import ast.SizeofExpression;
import ast.Statement;
import ast.StaticClassIdentifierExpression;
import ast.StringExpression;
import ast.VtableAccessExpression;
import ast.WhileStatement;
import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditRenameTypes;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImport.EffectiveImportType;
import import_mgmt.EffectiveImports;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import mbo.Renamings.RenamingIdKey;
import mbo.Vtables.ClassVtable;
import mbo.Vtables.VtableEntry;
import multifile.JsmntGlobal;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import transpile.BuiltinFunctionOverloads.FunctionOverloadReturn;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import typecheck.AbstractType;
import typecheck.ArrayType;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ImportType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.Pair;

public class TranspileProgram {
  public static final String LANG = "Csharp";

  private final Project project;
  private final Program program;
  private final NamespaceName moduleNamespace;
  private final String csharpFname;
  private final EffectiveImports effectiveImports;
  private final CsharpBuiltinFunctionOverloads builtinOverloads;

  private static final String INCREASE_INDENT = "  ";
  private String indent = "";

  private String progName;
  private String globalStatic = ""; // put into global class as static

  private Set<NamespaceName> namespaces = new HashSet<>();
  private String usingNamespaces = "";

  private String generatedTemplatesCsharp = "";

  private ClassDeclType rootObjType = null;
  private EnumDeclType cmpResultType = null;
  private String cmpEqId = "";

  public TranspileProgram(Project project, Program program, EffectiveImports effectiveImports,
      CsharpBuiltinFunctionOverloads builtinOverloads) {
    this.project = project;
    this.program = program;
    String projectName = project.dirPath();
    this.progName = program.moduleStatement.fullType.toString();
    this.csharpFname = "build-csharp/" + projectName + "/" + progName + ".cs";
    this.effectiveImports = effectiveImports;
    this.builtinOverloads = builtinOverloads.extend();

    moduleNamespace = new NamespaceName(project.getModProjectNameVersion(),
        program.moduleStatement.fullType);
  }

  public static FileOutputStream openFile(String fname) throws IOException {
    File outFile = new File(fname).getAbsoluteFile();
    if (!outFile.exists()) {
      String dir = outFile.getParent();
      if (dir != null) {
        new File(dir).mkdirs();
      }
      outFile.createNewFile();
    }

    System.out.println("  -> " + outFile);

    FileOutputStream fout = null;
    try {
      fout = new FileOutputStream(outFile);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalArgumentException(e.getMessage());
    }
    return fout;
  }

  private void getJsmntGlobalRenamings() throws FatalMessageException, TranspilerError {
    Nullable<ModuleEndpoint> modEp = project.lookupModule(JsmntGlobal.modType,
        new AuditTrailLite());
    if (modEp.isNull()) {
      throw new TranspilerError("unable to find jsmnt_global");
    }
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();

    Nullable<AuditEntry> globalRenames = moduleHandle.moduleBuildObj.auditTrail
        .getCombined(AuditEntryType.RENAME_TYPES);
    if (globalRenames.isNull()) {
      throw new TranspilerError("unable to find type renames");
    }
    AuditRenameTypes globalRenaming = (AuditRenameTypes) globalRenames.get();
    Type tmp = globalRenaming.typeRenamings
        .get(new EnumDeclType(Nullable.of(JsmntGlobal.modType), "CmpResult"));
    if (tmp == null) {
      throw new TranspilerError("unable to find cmp enum in type renames");
    }
    this.cmpResultType = (EnumDeclType) tmp;

    tmp = globalRenaming.typeRenamings
        .get(new ClassDeclType(Nullable.of(JsmntGlobal.modType), "Obj"));
    if (tmp == null) {
      throw new TranspilerError("unable to find Obj class in type renames");
    }
    this.rootObjType = (ClassDeclType) tmp;

    // now look up EQ
    RenamingIdKey idKey = new RenamingIdKey(
        Nullable.of(new EnumDeclType(Nullable.of(JsmntGlobal.modType), "CmpResult")),
        Nullable.empty(), "EQ");
    RenamingIdKey newIdKey = idKey.applyTransforms(moduleHandle.moduleBuildObj.auditTrail);

    this.cmpEqId = newIdKey.id;
  }

  public void transpileProgram() throws IOException, TranspilerError, FatalMessageException {
    FileOutputStream out = openFile(csharpFname);

    // find jsmnt global renames
    getJsmntGlobalRenamings();

    String result = "";

    // run through program lines and collect all declarations (class, fn, var).
    // transpile those first
    List<Ast> declarations = new LinkedList<Ast>();
    List<Ast> body = new LinkedList<Ast>();

    for (Ast ast : program.getLines()) {
      if (ast instanceof ClassDeclaration || ast instanceof Function || ast instanceof FfiStatement
          || ast instanceof EnumDeclaration) {
        declarations.add(ast);
      } else {
        body.add(ast);
      }
    }

    // add system imports
    result += "using System;\n";
    result += "using System.Linq;\n";
    result += "using System.Collections.Generic;\n";

    // add builtins
    result += "using csharp_builtin;\n";
    result += "using static csharp_builtin.GlobalStatic;\n";

    // figure out the namespaces we will use
    Set<EffectiveImportType> filterImpTypes = new HashSet<>();
    filterImpTypes.add(EffectiveImportType.LITERAL_IMPORT);
    filterImpTypes.add(EffectiveImportType.INHERITED_IMPORT);
    filterImpTypes.add(EffectiveImportType.IMPLIED_IMPORT);
    filterImpTypes.add(EffectiveImportType.PARENT_IMPORT);

    namespaces.add(builtinOverloads.builtinGenerator.namespaceName);
    namespaces.add(moduleNamespace);

    for (Entry<ImportType, EffectiveImport> entry : effectiveImports
        .getImportMap(true, filterImpTypes).entrySet()) {
      NamespaceName namespace = entry.getValue().getNamespaceName();
      if (namespaces.contains(namespace)) {
        continue;
      }
      namespaces.add(namespace);

      String alias = namespace.moduleType.toList().get(0);
      String tmp = "using " + alias + " = " + namespace.project + "." + alias + ";\n";
      if (!usingNamespaces.contains(tmp)) {
        usingNamespaces += tmp;
      }
      usingNamespaces += "using static " + namespace + ".GlobalStatic;\n";
    }

    result += usingNamespaces;

    result += "namespace " + moduleNamespace + "{\n";
    result += INCREASE_INDENT + "using static GlobalStatic;\n";

    result += transpileGlobalDeclarations(declarations);
    globalStatic += transpileMain();

    result += INCREASE_INDENT + "static class GlobalStatic {\n" + globalStatic
        + generatedTemplatesCsharp + "\n}\n";

    result += "\n}\n";

    out.write(result.getBytes());
  }

  private String transpileMain() throws TranspilerError {
    String localIndent = indent + INCREASE_INDENT + INCREASE_INDENT;
    String out = localIndent + "public static void Main(string[] argv) {\n";
    String fnIndent = localIndent + INCREASE_INDENT;

    Nullable<Function> main = program.getMainOrStart(Function.MetaType.START);
    if (main.isNull()) {
      return "";
    }

    String mainFn = fnIndent + main.get().name;
    if (main.get().fullType.argTypes.isEmpty()) {
      mainFn += "();";
    } else {
      // TODO pass in command line
      out += fnIndent + "CustomList<string> argvVec = new CustomList<string>();\n";
      out += fnIndent + "for (int n = 0; n < argv.Length; n++) {\n";
      out += fnIndent + INCREASE_INDENT + "argvVec.Add(argv[n]);\n";
      out += fnIndent + "}\n";
      mainFn += "(argvVec);";
    }
    out += mainFn + "\n";
    out += localIndent + "}\n";

    return out;
  }

  private String transpileGlobalDeclarations(List<Ast> declarations)
      throws TranspilerError, FatalMessageException {
    String out = "";

    // transpile all enums first, since they have no dependency on anything else
    for (Ast ast : declarations) {
      if (ast instanceof EnumDeclaration) {
        out += transpileEnumDeclaration((EnumDeclaration) ast) + "\n";
      }
    }

    for (Ast ast : declarations) {
      if (ast instanceof ClassDeclaration) {
        out += transpileClassDeclaration((ClassDeclaration) ast) + "\n";
      } else if (ast instanceof Statement) {

        String saveIndent = indent;
        if (ast instanceof Function) {
          indent += INCREASE_INDENT + INCREASE_INDENT;
        }

        String tmp = transpileStatement((Statement) ast);
        if (ast instanceof FfiStatement) {
          out += tmp;
        } else {
          if (ast instanceof Function) {
            tmp = INCREASE_INDENT + INCREASE_INDENT + "public static " + tmp;
          } else {
            tmp += ";";
          }
          globalStatic += tmp;
        }

        indent = saveIndent;

      } else if (ast instanceof Function) {
        throw new IllegalArgumentException("function is a statement");
      } else if (ast instanceof EnumDeclaration) {
        continue;
      }
    }

    return out;
  }

  private String transpileClassDeclaration(ClassDeclaration cdecl)
      throws TranspilerError, FatalMessageException {
    String result = "";

    // lookup the overload names
    FunctionOverloadReturn tmpStackOverload = builtinOverloads.lookupOverload(namespaces,
        CsharpBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, cdecl.fullType.asDeclType()).get();
    FunctionOverloadReturn tmpPtrOverload = builtinOverloads.lookupOverload(namespaces,
        CsharpBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, new ReferenceType(cdecl.fullType)).get();
    String stackOverload = tmpStackOverload.getNewName().a;
    String ptrOverload = tmpPtrOverload.getNewName().a;
    final String localIndent = indent + INCREASE_INDENT + INCREASE_INDENT;

    globalStatic += localIndent + "public static string " + stackOverload + "(" + cdecl.name
        + " item){\n" + localIndent + INCREASE_INDENT + "return \"<class instance>\";\n"
        + localIndent + "}\n";
    globalStatic += localIndent + "public static string " + ptrOverload + "(" + cdecl.name
        + " item){\n" + localIndent + INCREASE_INDENT
        + "return \"<reference::\" + item.refStr + \">\";\n" + localIndent + "}\n";

    // handle generics (these should be removed)
    if (cdecl.genericTypeNames.size() > 0) {
      throw new TranspilerError("All generics should be removed: " + cdecl);
    }

    final String saveIndent = indent;
    indent += INCREASE_INDENT;
    final String classIndent = indent;

    result += indent + "public class " + cdecl.name;
    if (cdecl.parent.isNotNull()) {
      result += " : " + transpileType(cdecl.parent.get());
    }

    result += " {\n";
    indent += INCREASE_INDENT;

    // handle refStr
    if (cdecl.parent.isNull()) {
      // add refStr as const string (could be enum) if no parent, along with
      // setRefStr()
      result += indent + "public string refStr = \"" + transpileType(cdecl.fullType) + "\";\n";
    }

    String retTyStr = transpileType(cdecl.fullType);
    result += indent + "public ";
    if (cdecl.parent.isNotNull()) {
      result += "new ";
    }
    result += retTyStr + " setRefStr(string str) {\n" + indent + INCREASE_INDENT + INCREASE_INDENT
        + "refStr = str;\n" + indent + INCREASE_INDENT + INCREASE_INDENT + "return this;\n" + indent
        + "}\n";

    List<DeclarationStatement> initFields = new LinkedList<>(); // just us

    String hashFnName = "";
    String cmpFnName = "";

    // now handle main class lines
    for (ClassLine cline : cdecl.lines) {
      if (cline.ast instanceof DeclarationStatement) {
        DeclarationStatement decl = (DeclarationStatement) cline.ast;
        result += indent + "public ";
        if (decl.getIsStatic()) {
          result += "static ";
        } else {
          initFields.add(decl);
        }
        result += transpileDeclarationStatement(decl) + ";\n";
      } else if (cline.ast instanceof Function) {
        Function fn = (Function) cline.ast;
        result += indent + "public ";
        if (fn.getIsStatic()) {
          result += "static ";
        }
        if (fn.isOverride) {
          result += "new ";
        }

        IdentifierExpression selfExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
            new LinkedList<>(), "this");
        selfExpr.setDeterminedType(new ReferenceType(cdecl.fullType));

        if (fn.metaType == MetaType.CONSTRUCTOR) {
          // add parent constructor call
          if (fn.parentCall.isNotNull()) {
            // after the rename, the parentCall expression will be a regular identifier, not
            // "parent"
            if (fn.parentCall.get().prefix.isNotNull()) {
              throw new IllegalArgumentException();
            }
            FunctionCallExpression fnCallExpr = new FunctionCallExpression(Nullable.of(selfExpr),
                fn.parentCall.get().expression, fn.parentCall.get().arguments,
                fn.parentCall.get().originalName);
            fnCallExpr.setDeterminedType(VoidType.Create());
            FunctionCallStatement pFnCall = new FunctionCallStatement(fnCallExpr);
            pFnCall.setDeterminedType(VoidType.Create());

            fn.getBody().get().statements.add(0, pFnCall);
          }

          // replace any 'return;' statements with 'return selfExpr;'
          ReturnStatement returnSelf = new ReturnStatement(Nullable.of(selfExpr));
          BlockStatement fnBody = fn.getBody().get();
          fnBody = (BlockStatement) fnBody.replace(new ReturnStatement(Nullable.empty()),
              returnSelf);
          fnBody.statements.add(returnSelf);

          // change the return type
          Function tmp = new Function(fn.lineNum, fn.columnNum, new LinkedList<>(),
              new LinkedList<>(), fn.export, fn.required, fn.isOverride, fn.name,
              new ClassDeclType(Nullable.empty(), fn.name),
              new FunctionType(fn.fullType.within, new ReferenceType(cdecl.fullType),
                  fn.fullType.argTypes, LambdaStatus.NOT_LAMBDA),
              fn.params, Nullable.of(fnBody), fn.getIsStatic(), fn.parentCall, fn.metaType);
          tmp.setScoping(fn.getScoping());
          tmp.setOriginalLabel(fn.getOriginalLabel());
          tmp.setOrdering(fn.getOrdering());
          fn = tmp;
        } else if (fn.metaType == Function.MetaType.HASH) {
          hashFnName = fn.name;
        } else if (fn.metaType == Function.MetaType.CMP) {
          cmpFnName = fn.name;
        }

        result += transpileFunction(fn) + "\n";
      } else if (cline.ast instanceof FfiStatement) {
        result += indent + transpileFfiStatement((FfiStatement) cline.ast) + "\n";
      }
    }

    // add hashcode
    if (!hashFnName.isEmpty()) {
      result += "public override int GetHashCode() {\n" + "  return " + hashFnName + "();\n"
          + "}\n";
    }
    // add equals function
    if (!cmpFnName.isEmpty()) {
      result += "public override bool Equals(Object obj) {\n" + "if (!(obj is " + rootObjType
          + ")) {return false;}\n" + "  return " + cmpFnName + "((" + rootObjType + ")obj) == "
          + cmpResultType + "." + cmpEqId + ";\n" + "}\n";
    }

    // add vtable stuff
    Nullable<ModuleEndpoint> modEp = project.lookupModule(program.moduleStatement.fullType,
        project.getAuditTrail());
    if (modEp.isNull()) {
      throw new TranspilerError(
          "internal error: failed to lookup module: " + program.moduleStatement.fullType);
    }
    try {
      modEp.get().load();
    } catch (FatalMessageException e) {
      throw new TranspilerError("internal error while loading module: " + e);
    }
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    // find our vtable
    ClassDeclType absName = new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name);
    ClassVtable vtable = moduleHandle.moduleBuildObj.vtables.get(absName);
    if (vtable == null) {
      throw new TranspilerError("internal error: Did not find vtable to copy for: " + absName);
    }

    int vIndex = 0;
    for (VtableEntry entry : vtable.vtableEntries) {
      result += indent + "public ";
      if (!entry.isRootDecl) {
        result += "override ";
      } else {
        result += "virtual ";
      }
      result += transpileType(entry.getType().returnType) + " ";

      String fnType = "callVtable_" + vIndex + "(";
      String fnCall = indent + INCREASE_INDENT;
      if (!(entry.getType().returnType instanceof VoidType)) {
        fnCall += "return ";
      }
      fnCall += entry.getName() + "(";
      int pIndex = 0;
      for (Type paramTy : entry.getType().argTypes) {
        if (pIndex != 0) {
          fnType += ", ";
          fnCall += ", ";
        }
        fnType += transpileType(paramTy) + " p" + pIndex;
        fnCall += "p" + pIndex;
        pIndex++;
      }
      fnType += ")";
      fnCall += ");";

      result += fnType + " {\n" + fnCall + "\n" + indent + "}\n";

      vIndex++;
    }

    // add a copy function
    {
      // collect parents' declarations (all unique after renamings)
      result += indent + "public ";
      if (cdecl.parent.isNotNull()) {
        result += "new ";
      }
      result += cdecl.name + " copy() {                                                    \n"
          + indent + INCREASE_INDENT + "var newInstance = new " + cdecl.name + "();        \n"
          + indent + INCREASE_INDENT + "copyInto(newInstance);                             \n"
          + indent + INCREASE_INDENT + "return newInstance;                                \n"
          + indent + "}\n";

      // add copyInto
      result += indent + "public void copyInto(" + cdecl.name + " newInstance) {           \n";
      if (cdecl.parent.isNotNull()) {
        result += indent + INCREASE_INDENT + "base.copyInto(newInstance);                  \n";
      }
      result += indent + INCREASE_INDENT + "newInstance.refStr = refStr;                   \n";
      for (DeclarationStatement field : initFields) {
        result += indent + INCREASE_INDENT + "newInstance." + field.name + " = " + field.name;
        if (field.type instanceof UserInstanceType || field.type instanceof ArrayType
            || field.type instanceof MapType) {
          result += ".copy()";
        }
        result += ";\n";
      }
      result += indent + "}\n";
    }

    result += "\n" + classIndent + "}\n";
    indent = saveIndent;
    return result;
  }

  private String transpileEnumDeclaration(EnumDeclaration enumDecl) {
    String result = "";

    // definedTypes.add(enumDecl.enumType.name);

    // result += usingNamespaces;
    result += "public enum " + enumDecl.enumType.name + "{";
    boolean first = true;
    for (String id : enumDecl.enumIds) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += id;
    }
    result += "};\n";

    // add copy() as an extension method
    final String localIndent = indent + INCREASE_INDENT + INCREASE_INDENT;
    globalStatic += localIndent + "public static " + enumDecl.enumType.name + " copy(this "
        + enumDecl.enumType.name + " item) {\n";
    globalStatic += localIndent + INCREASE_INDENT + "return item;\n";
    globalStatic += localIndent + "}\n";

    return result;
  }

  private String transpileStatement(Statement ast) throws TranspilerError {
    if (!(ast instanceof AbstractStatement)) {
      throw new TranspilerError("transpileStatement ast is not an instance of AbstractStatement");
    }
    AbstractStatement node = (AbstractStatement) ast;
    switch (node.astType) {
      case FUNCTION:
        return transpileFunction((Function) ast) + "\n";
      case ASSIGNMENT_STATEMENT:
        return transpileAssignmentStatement((AssignmentStatement) node);
      case BLOCK_STATEMENT:
        return transpileBlockStatement((BlockStatement) node);
      case BREAK_STATEMENT:
        return "break";
      case CONDITIONAL_STATEMENT:
        return transpileConditionalStatement((ConditionalStatement) node);
      case CONTINUE_STATEMENT:
        return "continue";
      case DECLARATION_STATEMENT:
        return transpileDeclarationStatement((DeclarationStatement) node);
      case FOR_STATEMENT:
        return transpileForStatement((ForStatement) node);
      case FUNCTION_CALL_STATEMENT:
        return transpileFunctionCallExpression(
            (FunctionCallExpression) ((FunctionCallStatement) node).fnExpression);
      case RETURN_STATEMENT:
        return transpileReturnStatement((ReturnStatement) node);
      case WHILE_STATEMENT:
        return transpileWhileStatement((WhileStatement) node);
      case FFI_STATEMENT:
        return transpileFfiStatement((FfiStatement) node);
      case EXPRESSION_STATEMENT:
        return transpileExpression(((ExpressionStatement) node).expression);
      case DELETE_STATEMENT:
        return transpileDeleteStatement((DeleteStatement) node);
      default:
        break;
    }
    return "/*<Error: " + node.astType + ">*/";
  }

  private String transpileDeleteStatement(DeleteStatement node) throws TranspilerError {
    return "";
  }

  private String transpileFfiStatement(FfiStatement node) {
    String val = node.literal.value;
    return val;
  }

  private String transpileWhileStatement(WhileStatement node) throws TranspilerError {
    String result = "while (" + convertToBool(node.guard) + ") ";
    result += transpileStatement(node.body) + "\n";
    return result;
  }

  private String transpileReturnStatement(ReturnStatement node) throws TranspilerError {
    String result = "return";
    if (node.returnValue.isNotNull()) {
      result += " " + transpileExpression(node.returnValue.get());
    }
    return result;
  }

  private String transpileForStatement(ForStatement node) throws TranspilerError {
    String result = "for (";
    if (node.init.isNotNull()) {
      result += transpileStatement(node.init.get());
    }
    result += "; " + convertToBool(node.guard) + "; ";
    if (node.action.isNotNull()) {
      result += transpileStatement(node.action.get());
    }
    result += ") " + transpileStatement(node.body);
    return result;
  }

  private String transpileDeclarationStatement(DeclarationStatement node) throws TranspilerError {
    String result = transpileType(node.type) + " " + node.name;
    if (node.getValue().isNotNull()) {
      result += " = " + attachCopyMethod(node.getValue().get().getDeterminedType(),
          transpileExpression(node.getValue().get()));
    }
    return result;
  }

  private String transpileConditionalStatement(ConditionalStatement node) throws TranspilerError {
    String result = "if (" + convertToBool(node.guard) + ") ";
    result += transpileStatement(node.thenBlock);
    if (node.elsBlock.isNotNull()) {
      result += " else " + transpileStatement(node.elsBlock.get());
    }
    return result;
  }

  private String transpileAssignmentStatement(AssignmentStatement node) throws TranspilerError {
    String targetStr = transpileLvalue(node.target);
    String sourceStr = attachCopyMethod(node.source.getDeterminedType(),
        transpileExpression(node.source));

    if (node.target instanceof LvalueDereference) {
      return "(" + sourceStr + ").copyInto(" + targetStr + ")";
    }
    return targetStr + " = " + sourceStr;
  }

  private String transpileBlockStatement(BlockStatement node) throws TranspilerError {
    String result = indent + "}"; // last brace first
    String save_indent = indent;
    indent += INCREASE_INDENT;

    // iterate bottom up with option for others to insert statements
    for (int i = node.statements.size() - 1,
        j = 0; i >= 0; j++, i = node.statements.size() - 1 - j) {
      Statement statement = node.statements.get(i);

      String tmp = transpileStatement(statement);
      if (!tmp.isEmpty()) {
        tmp = indent + tmp;
        if (!(statement instanceof ConditionalStatement) && !(statement instanceof BlockStatement)
            && !(statement instanceof FfiStatement)) {
          tmp += ";";
        }
        tmp += "\n";
        result = tmp + result;
      }
    }

    indent = save_indent;
    result = "{\n" + result; // first brace last
    return result;
  }

  private String transpileFunction(Function fn) throws TranspilerError {
    String result = "";
    result += transpileType(fn.fullType.returnType) + " ";
    result += fn.name + "(";

    result += transpileParams(fn.params);
    result += ")";
    result += " " + transpileBlockStatement(fn.getBody().get());
    return result;
  }

  private String transpileParams(List<DeclarationStatement> params) throws TranspilerError {
    String result = "";
    boolean first = true;
    for (DeclarationStatement param : params) {
      if (!first)
        result += ", ";
      first = false;
      result += wrapIdWithType(param.name, param.type);
    }
    return result;
  }

  private String wrapIdWithType(String id, Type type) throws TranspilerError {
    String result = transpileType(type);
    /*
     * if (type instanceof MapType || type instanceof ArrayType) { result = "ref " +
     * result; }
     */
    return result + " " + id;
  }

  private String transpileLvalue(Lvalue ast) throws TranspilerError {
    if (!(ast instanceof AbstractLvalue)) {
      throw new TranspilerError("transpileLvalue ast is not an instance of AbstractLvalue");
    }
    AbstractLvalue node = (AbstractLvalue) ast;
    switch (node.astType) {
      case ID_LVALUE:
        return transpileLvalueId((LvalueId) node);
      case BRACKET_LVALUE:
        return transpileLvalueBracket((LvalueBracket) node);
      case DOT_LVALUE:
        return transpileLvalueDot((LvalueDot) node);
      case DEREFERENCE_LVALUE:
        return transpileLvalueDereference((LvalueDereference) node);
      case FFI_LVALUE:
        return transpileLvalueFfi((LvalueFfi) node);
      case MULTI_LVALUE:
        return transpileMultiLvalue((MultiLvalue) node);
      default:
        break;
    }
    return "/*<Error: " + node.astType + ">*/";
  }

  private String transpileMultiLvalue(MultiLvalue node) throws TranspilerError {
    String res = "(";
    boolean first = true;
    for (Lvalue lvalue : node.lvalues) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += transpileLvalue(lvalue);
    }
    return res += ")";
  }

  private String transpileLvalueFfi(LvalueFfi node) {
    return node.literal.value;
  }

  private String transpileLvalueDereference(LvalueDereference node) throws TranspilerError {
    return transpileExpression(node.dereferenced);
  }

  private String transpileLvalueDot(LvalueDot node) throws TranspilerError {
    String dot = ".";
    return transpileExpression(node.left) + dot + node.id;
  }

  private String transpileLvalueBracket(LvalueBracket node) throws TranspilerError {
    String result = "";
    if (node.left.getDeterminedType() instanceof ReferenceType) {
      result += "(" + transpileExpression(node.left) + ")";
    } else {
      result += transpileExpression(node.left);
    }
    result += "[" + transpileExpression(node.index) + "]";
    return result;
  }

  private String transpileLvalueId(LvalueId node) throws TranspilerError {
    return node.id;
  }

  private String transpileExpression(Ast ast) throws TranspilerError {
    if (!(ast instanceof AbstractExpression)) {
      throw new TranspilerError("transpileExpression ast is not an instance of AbstractExpression");
    }
    AbstractExpression node = (AbstractExpression) ast;
    switch (node.astType) {
      case ARRAY_INIT_EXPRESSION:
        return transpileArrayInitExpression((ArrayInitExpression) node);
      case BINARY_EXPRESSION:
        return transpileBinaryExpression((BinaryExpression) node);
      case BOOL_EXPRESSION:
        return transpileBoolExpression((BoolExpression) node);
      case BRACKET_EXPRESSION:
        return transpileBracketExpression((BracketExpression) node);
      case CHAR_EXPRESSION:
        return transpileCharExpression((CharExpression) node);
      case DOT_EXPRESSION:
        return transpileDotExpression((DotExpression) node);
      case DOUBLE_EXPRESSION:
        return transpileFloatExpression((FloatExpression) node);
      case FUNCTION_CALL_EXPRESSION:
        return transpileFunctionCallExpression((FunctionCallExpression) node);
      case IDENTIFIER_EXPRESSION:
        return transpileIdentifierExpression((IdentifierExpression) node);
      case INTEGER_EXPRESSION:
        return transpileIntegerExpression((IntegerExpression) node);
      case LAMBDA_EXPRESSION:
        return transpileLambdaExpression((LambdaExpression) node);
      case LEFT_UNARY_OP_EXPRESSION:
        return transpileLeftUnaryOpExpression((LeftUnaryOpExpression) node);
      case MAP_INIT_EXPRESSION:
        return transpileMapInitExpression((MapInitExpression) node);
      case NULL_EXPRESSION:
        return "null";
      case STRING_EXPRESSION:
        return ((StringExpression) node).unparsed;
      case REFERENCE_EXPRESSION:
        return transpileReferenceExpression((ReferenceExpression) node);
      case DEREFERENCE_EXPRESSION:
        return "(" + transpileExpression(((DereferenceExpression) node).dereferenced) + ")";
      case THIS_EXPRESSION:
        return "this";
      case VTABLE_ACCESS_EXPRESSION:
        return "callVtable_" + ((VtableAccessExpression) node).index;
      case NEW_CLASS_INSTANCE_EXPRESSION:
        return transpileNewClassInstanceExpression((NewClassInstanceExpression) node);
      case NEW_EXPRESSION:
        // node.value must check it's parent
        return transpileExpression(((NewExpression) node).value);
      case CAST_EXPRESSION:
        return transpileCastExpression((CastExpression) node);
      case FFI_EXPRESSION:
        return transpileFfiExpression((FfiExpression) node);
      case MULTI_EXPRESSION:
        return transpileMultiExpression((MultiExpression) node);
      case STATIC_CLASS_IDENTIFIER_EXPRESSION:
        return transpileStaticClassIdentifierExpression((StaticClassIdentifierExpression) node);
      case SIZEOF_EXPRESSION:
        return ((SizeofExpression) node).getCalculatedSize() + "";
      case INSTANCEOF_EXPRESSION:
        return transpileInstanceofExpression((InstanceofExpression) node);
      default:
        break;
    }
    return "/*<Error: " + node.astType + ">*/";
  }

  private String transpileInstanceofExpression(InstanceofExpression node) throws TranspilerError {
    String result = "";
    switch (node.operator) {
      case INSTANCEOF:
        break;
      case NOT_INSTANCEOF:
        result += "!";
        break;
      default:
        throw new TranspilerError("unknown operator: " + node.operator);
    }
    result += "(";
    result += transpileExpression(node.left) + " is " + transpileType(node.right);
    result += ")";
    return "(" + result + ")";
  }

  private String transpileStaticClassIdentifierExpression(StaticClassIdentifierExpression node) {
    return node.id.toString();
  }

  private String transpileMultiExpression(MultiExpression node) throws TranspilerError {
    String values = "(";
    boolean first = true;
    for (Expression expr : node.expressions) {
      if (!first) {
        values += ", ";
      }
      first = false;
      values += transpileExpression(expr);
    }
    return values + ")";
  }

  private String transpileFfiExpression(FfiExpression node) {
    return node.literal.value;
  }

  private String transpileCastExpression(CastExpression node) throws TranspilerError {
    return "((" + transpileType(node.target) + ")(" + transpileExpression(node.value) + "))";
  }

  private String transpileReferenceExpression(ReferenceExpression node) throws TranspilerError {
    return "(" + transpileExpression(node.referenced) + ")";
  }

  private String transpileNewClassInstanceExpression(NewClassInstanceExpression node)
      throws TranspilerError {
    String result = "";
    result += "((" + "(" + transpileType(node.getName()) + ")" + "(new ";
    result += transpileType(node.getName()) + "()";
    result += ").";

    // constructorCall always returns ptr
    result += transpileExpression(node.constructorCall);

    // change refStr for this instance for printing
    result += ").setRefStr(\"" + node.originalName + "\")";
    result += ")";

    return result;
  }

  private String transpileMapInitExpression(MapInitExpression node) throws TranspilerError {
    String result = "new " + transpileType(node.getDeterminedType()) + "{";
    boolean first = true;
    for (Entry<Expression, Expression> pair : node.pairs.entrySet()) {
      if (!first)
        result += ",\n";
      first = false;

      result += "{"
          + attachCopyMethod(pair.getKey().getDeterminedType(), transpileExpression(pair.getKey()));
      result += ", ";
      result += attachCopyMethod(pair.getValue().getDeterminedType(),
          transpileExpression(pair.getValue())) + "}";
    }
    return result + "}";
  }

  private String transpileLeftUnaryOpExpression(LeftUnaryOpExpression node) throws TranspilerError {
    String result = "(" + node.operator.leftUnaryOpName;
    boolean convertToBool = node.getDeterminedType() instanceof BoolType
        && !(node.right.getDeterminedType() instanceof BoolType);
    if (convertToBool) {
      result += convertToBool(node.right);
    } else {
      result += transpileExpression(node.right);
    }
    return result + ")";
  }

  private String transpileIntegerExpression(IntegerExpression node) {
    return node.value;
  }

  private String transpileIdentifierExpression(IdentifierExpression node) throws TranspilerError {
    if (node.id.equals("print")) {
      return "custom_print";
    }
    return node.id;
  }

  private String transpileLambdaExpression(LambdaExpression node) throws TranspilerError {
    /* capture by copy */;

    // get free variables and add them as parameters, unless they are globals
    Map<String, Type> freeVars = node.findFreeVariables();
    freeVars.remove("print");
    freeVars.remove("currentUnixEpochTime");

    /*
     * ((fv1, fv2) => {return ((p1, p2) => {});})(fv1, fv2)
     */

    String innerLambda = "(";
    boolean first = true;
    for (DeclarationStatement param : node.params) {
      if (!first) {
        innerLambda += ", ";
      }
      first = false;
      innerLambda += wrapIdWithType(param.name, param.type);
    }
    innerLambda += ") => " + transpileBlockStatement(node.body) + "))";

    String outerLambdaFuncType = "(Func<";
    String outerLambda = "((";
    String callOuterLambda = "(";
    first = true;
    for (Entry<String, Type> fvEntry : freeVars.entrySet()) {
      if (!first) {
        outerLambda += ", ";
        callOuterLambda += ", ";
      }
      outerLambdaFuncType += transpileType(fvEntry.getValue()) + ", ";
      outerLambda += wrapIdWithType(fvEntry.getKey(), fvEntry.getValue());
      callOuterLambda += fvEntry.getKey();
    }
    outerLambdaFuncType += transpileType(node.getDeterminedType()) + ">)";
    outerLambda += ") => {\n" + indent + "return ((" + transpileType(node.getDeterminedType())
        + ") (" + innerLambda + ";\n" + indent + "})";
    callOuterLambda += ")";

    String result = "((" + outerLambdaFuncType + outerLambda + ")" + callOuterLambda + ")";

    return result;
  }

  private String transpileFloatExpression(FloatExpression node) {
    return node.value;
  }

  private String transpileDotExpression(DotExpression node) throws TranspilerError {
    String result = transpileExpression(node.left);
    return result + "." + transpileExpression(node.right);
  }

  private String transpileCharExpression(CharExpression node) {
    return node.unparsed;
  }

  private String transpileBracketExpression(BracketExpression node) throws TranspilerError {
    String result = "";
    if (node.left.getDeterminedType() instanceof ReferenceType) {
      result += "(" + transpileExpression(node.left) + ")";
    } else {
      result += transpileExpression(node.left);
    }
    return result + "[" + transpileExpression(node.index) + "]";
  }

  private String transpileBoolExpression(BoolExpression node) {
    return node.value ? "true" : "false";
  }

  private String transpileBinaryExpression(BinaryExpression node) throws TranspilerError {
    String op = node.operator.binOpName;
    boolean ffiConcat = false;
    if (op.equals("#")) {
      ffiConcat = true;
    }

    String result = "";
    if (!ffiConcat) {
      result += "(";
    }

    String comparingCollections = "";
    if ((node.operator.equals(BinaryExpression.BinOp.NOT_EQUAL)
        || node.operator.equals(BinaryExpression.BinOp.EQUAL))
        && (node.left.getDeterminedType() instanceof ArrayType
            || node.left.getDeterminedType() instanceof MapType)) {
      if (node.left.getDeterminedType() instanceof ArrayType) {
        comparingCollections = ".SequenceEqual(";
      } else {
        comparingCollections = ".Equals(";
      }
      if (node.operator.equals(BinaryExpression.BinOp.NOT_EQUAL)) {
        result += "!";
      }
    }

    String tmp = transpileExpression(node.left);
    if (node.right.getDeterminedType().equals(StringType.Create())
        && !node.left.getDeterminedType().equals(StringType.Create()) && !ffiConcat) {
      FunctionOverloadReturn tmpOverloadName = builtinOverloads.lookupOverload(namespaces,
          CsharpBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, node.left.getDeterminedType()).get();
      Pair<String, String> newNamePair = tmpOverloadName.getNewName();
      String overloadName = newNamePair.a;
      String generatedTemplate = newNamePair.b;
      generatedTemplatesCsharp += generatedTemplate;
      result += overloadName + "(" + tmp + ")";
    } else {
      result += tmp;
    }

    if (!ffiConcat) {
      if (!comparingCollections.isEmpty()) {
        result += comparingCollections;
      } else {
        result += " " + op + " ";
      }
    }

    tmp = transpileExpression(node.right);
    if (node.left.getDeterminedType().equals(StringType.Create())
        && !node.right.getDeterminedType().equals(StringType.Create()) && !ffiConcat) {
      FunctionOverloadReturn tmpOverloadName = builtinOverloads.lookupOverload(namespaces,
          CsharpBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, node.right.getDeterminedType()).get();
      Pair<String, String> newNamePair = tmpOverloadName.getNewName();
      String overloadName = newNamePair.a;
      String generatedTemplate = newNamePair.b;
      generatedTemplatesCsharp += generatedTemplate;
      result += overloadName + "(" + tmp + ")";
    } else {
      result += tmp;
    }

    if (!ffiConcat) {
      if (!comparingCollections.isEmpty()) {
        result += ")";
      }
      result += ")";
    }

    return result;
  }

  private String transpileArrayInitExpression(ArrayInitExpression node) throws TranspilerError {
    String result = "(new " + transpileType(node.getDeterminedType()) + "()";
    boolean first = true;
    if (!node.values.isEmpty()) {
      result += "{";
    }
    for (Expression val : node.values) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += attachCopyMethod(val.getDeterminedType(), transpileExpression(val));
    }
    if (!node.values.isEmpty()) {
      result += "}";
    }
    return result + ")";
  }

  private String transpileFunctionCallExpression(FunctionCallExpression node)
      throws TranspilerError {
    String result = "";

    if (node.prefix.isNotNull()) {
      Expression prefixExpr = node.prefix.get();
      String dot = ".";

      Type prefixType = prefixExpr.getDeterminedType();
      if (prefixType instanceof ReferenceType) {
        prefixType = ((ReferenceType) prefixType).innerType;
      }

      if ((prefixType instanceof ArrayType || prefixType instanceof MapType)
          && node.expression instanceof IdentifierExpression) {

        String name = ((IdentifierExpression) node.expression).id;
        if (name.equals("remove")) {
          return transpileExpression(prefixExpr) + ".CustomRemove("
              + transpileExpression(node.arguments.get(0)) + ")";
        }
        if (name.equals("add")) {
          if (node.arguments.size() == 1) {
            name = "Add";
          } else {
            if (prefixExpr.getDeterminedType() instanceof ArrayType) {
              return transpileExpression(prefixExpr) + ".Insert("
                  + transpileExpression(node.arguments.get(0)) + ", "
                  + transpileExpression(node.arguments.get(1)) + ")";
            } else {
              throw new IllegalArgumentException();
            }
          }
        }
        if (name.equals("put")) {
          return transpileExpression(prefixExpr) + ".Add("
              + transpileExpression(node.arguments.get(0)) + ", "
              + transpileExpression(node.arguments.get(1)) + ")";
        }
        if (name.equals("size")) {
          return transpileExpression(prefixExpr) + ".Count";
        }

        String argStr = "(";
        boolean first = true;
        for (Expression arg : node.arguments) {
          if (!first) {
            argStr += ", ";
          }
          first = false;
          argStr += transpileExpression(arg);
        }

        return transpileExpression(prefixExpr) + dot + name + argStr + ")";
      }

      if (prefixType instanceof ModuleType) {
        result += transpileExpression(prefixExpr) + ".GlobalStatic" + dot;
      } else {
        result += transpileExpression(prefixExpr) + dot;
      }
    }

    result += transpileExpression(node.expression) + "(";

    boolean first = true;
    for (Expression arg : node.arguments) {
      if (!first)
        result += ", ";
      first = false;
      result += attachCopyMethod(arg.getDeterminedType(), transpileExpression(arg));
    }
    return result + ")";
  }

  //////////////////// Convert types

  public static String transpileType(Type type) {
    if (!(type instanceof AbstractType)) {
      throw new IllegalArgumentException("transpileType type is not an instance of AbstractType");
    }
    String result = "";
    FunctionType fTy = null;
    ClassType uTy = null;
    ClassDeclType declTy = null;
    EnumType enumTy = null;
    EnumDeclType enumDeclTy = null;
    boolean first = true;

    AbstractType aType = (AbstractType) type;
    switch (aType.typeType) {
      case VOID_TYPE:
      case BOOL_TYPE:
      case CHAR_TYPE:
        return aType.toString().replaceAll("const ", "");
      case INT_TYPE:
        return "int";
      case FLOAT32_TYPE:
        return "float";
      case FLOAT64_TYPE:
        return "double";
      case STRING_TYPE:
        return "string";
      case ARRAY_TYPE:
        return "CustomList<" + transpileType(type.getInnerTypes().get(0)) + ">";
      case MAP_TYPE:
        return "CustomDictionary<" + transpileType(type.getInnerTypes().get(0)) + ", "
            + transpileType(type.getInnerTypes().get(1)) + ">";
      case FUNCTION_TYPE:
        fTy = (FunctionType) aType;
        // Func<p1, p1, ret>
        result = "Func<";
        for (Type ty : fTy.argTypes) {
          result += transpileType(ty);
          result += ", ";
        }
        result += transpileType(fTy.returnType) + ">";
        return result;
      case REFERENCE_TYPE:
        result = transpileType(((ReferenceType) aType).innerType);
        return result;
      case CLASS_DECL_TYPE:
        declTy = (ClassDeclType) aType;
        result = "";

        if (declTy.outerType.isNotNull()) {
          result += transpileType(declTy.outerType.get()) + ".";
        }

        result += declTy.name;
        return result;
      case CLASS_TYPE:
        uTy = (ClassType) aType;
        result = "";

        if (uTy.outerType.isNotNull()) {
          result += transpileType(uTy.outerType.get()) + ".";
        }

        result += uTy.name;
        if (uTy.innerTypes.size() > 0) {
          for (Type ty : uTy.innerTypes) {
            if (!first)
              result += ", ";
            first = false;
            result += transpileType(ty);
          }
        }
        return result; // this replace is for any nested types
      case ENUM_DECL_TYPE:
        enumDeclTy = (EnumDeclType) aType;
        result = "";

        if (enumDeclTy.outerType.isNotNull()) {
          result += transpileType(enumDeclTy.outerType.get()) + ".";
        }

        result += enumDeclTy.name;
        return result;
      case ENUM_TYPE:
        enumTy = (EnumType) aType;
        result = "";

        if (enumTy.outerType.isNotNull()) {
          result += transpileType(enumTy.outerType.get()) + ".";
        }

        result += enumTy.name;
        return result;
      case MULTI_TYPE:
        return transpileMultiType((MultiType) type);
      case MODULE_TYPE:
        return transpileModuleType((ModuleType) type);
      default:
        break;
    }
    return "/*<Error: " + aType.typeType + ">*/";
  }

  public static String transpileModuleType(ModuleType type) {
    List<String> names = type.toList();
    return String.join("::", names);
  }

  public static String transpileMultiType(MultiType type) {
    String types = "(";
    boolean first = true;
    for (Type ty : type.types) {
      if (!first) {
        types += ", ";
      }
      first = false;
      types += transpileType(ty);
    }
    return types + ")";
  }

  ///////////////////////////////////

  private boolean isFullCopy(Type type) {
    return (type instanceof UserInstanceType || type instanceof MapType
        || type instanceof ArrayType);
  }

  private String attachCopyMethod(Type type, String expr) {
    if (isFullCopy(type)) {
      return "((" + expr + ").copy())";
    }
    return expr;
  }

  private String convertToBool(Expression expr) throws TranspilerError {
    String tmp = transpileExpression(expr);
    if (expr.getDeterminedType() instanceof BoolType) {
      return tmp;
    }
    if (expr.getDeterminedType() instanceof CharType) {
      return "(" + tmp + " != 0)";
    }
    return "Convert.ToBoolean(" + tmp + ")";
  }

}
