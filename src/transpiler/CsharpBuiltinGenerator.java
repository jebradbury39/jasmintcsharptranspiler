package transpiler;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import transpile.BuiltinFunctionOverloads.FunctionOverloadReturn;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import transpile.BuiltinGenerator;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.Float32Type;
import typecheck.Float64Type;
import typecheck.IntType;
import typecheck.StringType;

public class CsharpBuiltinGenerator extends BuiltinGenerator {

  public static final String CSHARP_NAME = "builtin.cs";
  public static final String OVERLOAD_CUSTOM_TO_STRING = "custom_to_string";

  private String csharpResult = "";

  public CsharpBuiltinGenerator(String namespace) {
    super(namespace);
  }

  @Override
  public void generate() throws IOException {
    generatePrefix();
    generateMisc();
    generateCustomToString();
    csharpResult += "\n}\n";
    generateClasses();
    generateSuffix();

    String dirPath = "build-csharp/" + namespace + "/";
    outputToFile(csharpResult, dirPath + CSHARP_NAME);
  }

  private void outputToFile(String data, String filepath) throws IOException {
    FileOutputStream stream = TranspileProgram.openFile(filepath);
    stream.write(data.getBytes());
    stream.close();
  }

  private void generatePrefix() {
    csharpResult += "using System;\n";
    csharpResult += "using System.Collections.Generic;\n";
    csharpResult += "namespace " + namespace + "{\n";
    csharpResult += "public class GlobalStatic {\n";
  }

  private void generateSuffix() {
    csharpResult += "\n}\n";
  }

  private void generateClasses() {
    csharpResult += "public class CustomDictionary<K, V> : Dictionary<K, V> {     \n"
        + "  public override bool Equals(object other) {                          \n"
        + "    if (other == null) {                                               \n"
        + "      return false;                                                    \n"
        + "    }                                                                  \n"
        + "    if (!(other is CustomDictionary<K, V>)) {                          \n"
        + "      return false;                                                    \n"
        + "    }                                                                  \n"
        + "    CustomDictionary<K, V> ourOther = (CustomDictionary<K, V>) other;  \n"
        + "    foreach (var entry in this) {                                      \n"
        + "      V otherVal;                                                      \n"
        + "      if (ourOther.TryGetValue(entry.Key, out otherVal)) {             \n"
        + "        if (!entry.Value.Equals(otherVal)) {                           \n"
        + "          return false;                                                \n"
        + "        }                                                              \n"
        + "      } else {                                                         \n"
        + "        return false;                                                  \n"
        + "      }                                                                \n"
        + "    }                                                                  \n"
        + "    return true;                                                       \n"
        + "  }                                                                    \n"
        + "  public override int GetHashCode() {                                  \n"
        + "    int hash = 0;                                                      \n"
        + "    foreach (var entry in this) {                                      \n"
        + "      hash += entry.GetHashCode();                                     \n"
        + "    }                                                                  \n"
        + "    return hash;                                                       \n"
        + "  }                                                                    \n"
        + "  public V CustomRemove(K key) {                                       \n"
        + "    V val = this[key];                                                 \n"
        + "    Remove(key);                                                       \n"
        + "    return val;                                                        \n"
        + "  }                                                                    \n"
        + "  public CustomDictionary<K, V> copy() {                               \n"
        + "    CustomDictionary<K, V> newDict = new CustomDictionary<K, V>();     \n"
        + "    foreach (var entry in this) {                                      \n"
        + "      newDict.Add(entry.Key, entry.Value);                             \n"
        + "    }                                                                  \n"
        + "    return newDict;                                                    \n"
        + "  }                                                                    \n"
        + "  public void copyInto(CustomDictionary<K, V> newDict) {               \n"
        + "    newDict.Clear();                                                   \n"
        + "    foreach (var entry in this) {                                      \n"
        + "      newDict.Add(entry.Key, entry.Value);                             \n"
        + "    }                                                                  \n"
        + "  }                                                                    \n"
        + "}                                                                      \n";

    csharpResult += "public class CustomList<V> : List<V> {                                \n"
        + "   public override bool Equals(object other) {                                  \n"
        + "     if (other == null) {                                                       \n"
        + "       return false;                                                            \n"
        + "     }                                                                          \n"
        + "     if (!(other is CustomList<V>)) {                                           \n"
        + "       return false;                                                            \n"
        + "     }                                                                          \n"
        + "     CustomList<V> ourOther = (CustomList<V>) other;                            \n"
        + "     if (Count != ourOther.Count) {                                             \n"
        + "       return false;                                                            \n"
        + "     }                                                                          \n"
        + "     for (int idx = 0; idx < Count; idx += 1) {                                 \n"
        + "       if (!this[idx].Equals(ourOther[idx])) {                                  \n"
        + "         return false;                                                          \n"
        + "       }                                                                        \n"
        + "     }                                                                          \n"
        + "     return true;                                                               \n"
        + "  }                                                                             \n"
        + "                                                                                \n"
        + "  public override int GetHashCode() {                                           \n"
        + "    int hash = 0;                                                               \n"
        + "    foreach (var item in this) {                                                \n"
        + "      hash += item.GetHashCode();                                               \n"
        + "    }                                                                           \n"
        + "    return hash;                                                                \n"
        + "  }                                                                             \n"
        + "  public V CustomRemove(int index) {                                            \n"
        + "    V val = this[index];                                                        \n"
        + "    RemoveAt(index);                                                            \n"
        + "    return val;                                                                 \n"
        + "  }                                                                             \n"
        + "  public CustomList<V> copy() {                                                 \n"
        + "    CustomList<V> newList = new CustomList<V>();                                \n"
        + "    newList.AddRange(this);                                                     \n"
        + "    return newList;                                                             \n"
        + "  }                                                                             \n"
        + "  public void copyInto(CustomList<V> newList) {                                 \n"
        + "    newList.Clear();                                                            \n"
        + "    newList.AddRange(this);                                                     \n"
        + "  }                                                                             \n"
        + "}";
  }

  private void generateMisc() {
    csharpResult += "public static double currentUnixEpochTime() {\n"
        + "  return DateTimeOffset.Now.ToUnixTimeSeconds();\n}\n";
    csharpResult += "public static void custom_print(string val) {Console.Out.Write(val);}\n";
  }

  private void generateCustomToString() {
    Set<NamespaceName> usingNamespaces = new HashSet<>();
    usingNamespaces.add(namespaceName);

    FunctionOverloadReturn tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, StringType.Create()).get();
    String overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload + "(string str) {\n  return str;\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, BoolType.Create()).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(bool item) {\n  return item ? \"true\" : \"false\";\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, CharType.Create()).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(char item) {\n  return item + \"\";\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, Float32Type.Create()).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload + "(float item) {                    \n"
        + "  string tmp = item.ToString();                                                  \n"
        + "  if (!tmp.Contains(\".\")) {                                                      \n"
        + "    tmp += \".0\";                                                               \n"
        + "  }                                                                              \n"
        + "  return tmp;                                                                    \n"
        + "}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, Float64Type.Create()).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload + "(double item) {                  \n"
        + "  string tmp = item.ToString();                                                 \n"
        + "  if (!tmp.Contains(\".\")) {                                                     \n"
        + "    tmp += \".0\";                                                              \n"
        + "  }                                                                             \n"
        + "  return tmp;                                                                   \n"
        + "}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 8)).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(byte item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 16))
        .get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(UInt16 item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 32))
        .get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(UInt32 item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 64))
        .get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(UInt64 item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 8)).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(sbyte item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 16)).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(Int16 item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 32)).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(Int32 item) {\n  return item.ToString();\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 64)).get();
    overload = tmpOverload.getNewName().a;
    csharpResult += "public static string " + overload
        + "(Int64 item) {\n  return item.ToString();\n}\n";
  }

}
